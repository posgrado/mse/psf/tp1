#include "adc.h"
#include "uart.h"
#include "arm_math.h"

#define SAMPLES_IN_BUFFER 64
volatile uint8_t state = 1;

static const nrf_drv_timer_t m_timer = NRF_DRV_TIMER_INSTANCE(0);
static nrf_saadc_value_t m_buffer_pool[2][SAMPLES_IN_BUFFER];
static nrf_ppi_channel_t m_ppi_channel;
static uint32_t m_adc_evt_counter;

struct header_struct
{
    char pre[8];
    uint32_t id;
    uint16_t N;
    uint16_t fs;
    uint32_t maxIndex;
    uint32_t minIndex;
    q15_t maxValue;
    q15_t minValue;
    q15_t rms;
    char pos[4];
};

struct header_struct header = {"*header*", 0, 256, 5000, 0, 0, 0, 0, 0, "end*"};

void timer_handler(__attribute__((unused)) nrf_timer_event_t event_type,
                   __attribute__((unused)) void *p_context)
{
    // Intentionally unimplemented...
}

void saadc_sampling_event_init(void)
{
    ret_code_t err_code;

    err_code = nrf_drv_ppi_init();
    APP_ERROR_CHECK(err_code);

    nrf_drv_timer_config_t timer_cfg = NRF_DRV_TIMER_DEFAULT_CONFIG;
    timer_cfg.bit_width = NRF_TIMER_BIT_WIDTH_32;
    err_code = nrf_drv_timer_init(&m_timer, &timer_cfg, timer_handler);
    APP_ERROR_CHECK(err_code);

    /* setup m_timer for compare event every 200us */
    uint32_t ticks = nrf_drv_timer_us_to_ticks(&m_timer, 200);
    nrf_drv_timer_extended_compare(&m_timer,
                                   NRF_TIMER_CC_CHANNEL0,
                                   ticks,
                                   NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK,
                                   false);
    nrf_drv_timer_enable(&m_timer);

    uint32_t timer_compare_event_addr = nrf_drv_timer_compare_event_address_get(&m_timer,
                                                                                NRF_TIMER_CC_CHANNEL0);
    uint32_t saadc_sample_task_addr = nrf_drv_saadc_sample_task_get();

    /* setup ppi channel so that timer compare event is triggering sample task in SAADC */
    err_code = nrf_drv_ppi_channel_alloc(&m_ppi_channel);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_ppi_channel_assign(m_ppi_channel,
                                          timer_compare_event_addr,
                                          saadc_sample_task_addr);
    APP_ERROR_CHECK(err_code);
}

void saadc_sampling_event_enable(void)
{
    ret_code_t err_code = nrf_drv_ppi_channel_enable(m_ppi_channel);

    APP_ERROR_CHECK(err_code);
}

void saadc_callback(nrf_drv_saadc_evt_t const *p_event)
{
    static uint16_t sample = 0;
    static int16_t adc[256];

    if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
    {
        ret_code_t err_code;

        err_code = nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, SAMPLES_IN_BUFFER);
        APP_ERROR_CHECK(err_code);

        for (uint32_t i = 0; i < SAMPLES_IN_BUFFER; i++)
        {
            adc[sample] = p_event->data.done.p_buffer[i];
            // adc[sample] = adc[sample] >> 8;
            uartWriteByteArray((uint8_t *)&adc[sample], sizeof(adc[sample]));
            if (++sample == header.N)
            {
                sample = 0;
                header.id++;
                arm_max_q15(adc, header.N, &header.maxValue, &header.maxIndex);
                arm_min_q15(adc, header.N, &header.minValue, &header.minIndex);
                arm_rms_q15(adc, header.N, &header.rms);
                uartWriteByteArray((uint8_t *)&header, sizeof(header));
            }
        }
        m_adc_evt_counter++;
    }
}

void saadc_init(void)
{
    ret_code_t err_code = 0;

    uart_init(err_code);
    APP_ERROR_CHECK(err_code);

    nrf_saadc_channel_config_t channel_config =
        NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN4);

    err_code = nrf_drv_saadc_init(NULL, saadc_callback);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_channel_init(4, &channel_config);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[0], SAMPLES_IN_BUFFER);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[1], SAMPLES_IN_BUFFER);
    APP_ERROR_CHECK(err_code);
}