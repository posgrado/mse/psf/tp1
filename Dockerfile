# Don't use Alpine because needing running 32-bit ARM GCC compiler
FROM ubuntu:20.04

RUN apt-get update && \
	apt-get install -y  \
		curl \
		# Nedded to run arm-none-eabi-gcc
		gcc-multilib \
		unzip \
		make \
		ruby \
		build-essential && \
    gem install ceedling

ENV INSTALL="/nordic"

RUN mkdir -p ${INSTALL} && \
	cd ${INSTALL} && \
	curl https://developer.nordicsemi.com/nRF5_SDK/nRF5_SDK_v17.x.x/nRF5_SDK_17.0.2_d674dde.zip -o nRF5_SDK.zip && \
	unzip nRF5_SDK.zip && \
	rm nRF5_SDK.zip

ENV INSTALL="/arm-gcc"

RUN mkdir -p ${INSTALL} && \
	cd ${INSTALL} && \	
	curl https://armkeil.blob.core.windows.net/developer/Files/downloads/gnu-rm/10-2020q4/gcc-arm-none-eabi-10-2020-q4-major-x86_64-linux.tar.bz2 -o arm.tar.bz2 && \
    tar xf arm.tar.bz2 && \
	rm arm.tar.bz2 
