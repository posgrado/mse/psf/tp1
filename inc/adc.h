#include "nrf_drv_saadc.h"
#include "nrf_drv_ppi.h"
#include "nrf_drv_timer.h"
#include "boards.h"
#include "app_error.h"
#include "nrf_delay.h"
#include "app_util_platform.h"
#include "nrf_pwr_mgmt.h"

void timer_handler(nrf_timer_event_t event_type, void *p_context);

void saadc_sampling_event_init(void);

void saadc_sampling_event_enable(void);

void saadc_callback(nrf_drv_saadc_evt_t const *p_event);

void saadc_init(void);
