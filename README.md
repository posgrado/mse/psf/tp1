# Procesamiento de señales (fundamentos)

## Trabajo práctico 1

### Archivos

En la carpeta [`notebooks`](notebooks) se encuentran los Jupyter _notebooks_ con los ejercicios de Python.

### Prerequisitos

Para compilar este repositorio es necesario tener:

- Docker
- nRF Command Line Tools
