#!python3
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import serial

STREAM_FILE = ("/dev/tty.usbmodem0007600901961", "serial")

header = {"pre": b"*header*", "id": 0, "N": 256, "fs": 5000, "maxIndex": 0,
          "minIndex": 0, "maxValue": 0, "minValue": 0, "rms": 0, "pos": b"end*"}
fig = plt.figure(1)

adcAxe = fig.add_subplot(2, 1, 1)
adcLn, = plt.plot([], [], 'r-', linewidth=4)
minValueLn, = plt.plot([], [], 'g-', linewidth=2, alpha=0.3)
maxValueLn, = plt.plot([], [], 'y-', linewidth=2, alpha=0.3)
rmsLn, = plt.plot([], [], 'b-', linewidth=2, alpha=0.3)
minIndexLn, = plt.plot([], [], 'go', linewidth=6, alpha=0.8)
maxIndexLn, = plt.plot([], [], 'yo', linewidth=6, alpha=0.8)
adcAxe.grid(True)
adcAxe.set_ylim(-1.65, 1.65)

fftAxe = fig.add_subplot(2, 1, 2)
fftLn, = plt.plot([], [], 'b-', linewidth=4)
fftAxe.grid(True)
fftAxe.set_ylim(0, 0.25)


def find_header(f, h):
    data = bytearray(b'12345678')
    while data != h["pre"]:
        data += f.read(1)
        if len(data) > len(h["pre"]):
            del data[0]
    h["id"] = read_int4file(f, 4)
    h["N"] = read_int4file(f)
    h["fs"] = read_int4file(f)
    h["maxIndex"] = read_int4file(f, 4)
    h["minIndex"] = read_int4file(f, 4)
    h["maxValue"] = (read_int4file(f, sign=True)*1.65)/(1024)
    h["minValue"] = (read_int4file(f, sign=True)*1.65)/(1024)
    h["rms"] = (read_int4file(f, sign=True)*1.65)/(1024)

    data = bytearray(b'1234')
    while data != h["pos"]:
        data += f.read(1)
        if len(data) > len(h["pos"]):
            del data[0]
    print(h)
    return h["id"], h["N"], h["fs"], h["minValue"], h["maxValue"], h["rms"], h["minIndex"], h["maxIndex"]


def read_int4file(f, size=2, sign=False):
    raw = f.read(1)
    while(len(raw) < size):
        raw += f.read(1)
    return (int.from_bytes(raw, "little", signed=sign))


def read_samples(adc, n, trigger=False, th=0):
    state = "waitLow" if trigger else "sampling"
    i = 0
    for _ in range(n):
        sample = (read_int4file(streamFile, sign=True)-490)/512*1.65
        state, next_i = {
            "waitLow": lambda sample, i: ("waitHigh", 0) if sample < th else ("waitLow", 0),
            "waitHigh": lambda sample, i: ("sampling", 0) if sample > th else ("waitHigh", 0),
            "sampling": lambda sample, i: ("sampling", i+1)
        }[state](sample, i)
        adc[i] = sample
        i = next_i


def update(t):
    global header
    _, N, fs, minValue, maxValue, rms, minIndex, maxIndex = find_header(
        streamFile, header)
    adc = np.zeros(N)
    time = np.arange(0, N/fs, 1/fs)
    read_samples(adc, N, True, 0.1)

    adcAxe.set_xlim(0, N/fs)
    adcLn.set_data(time, adc)
    minValueLn.set_data(time, minValue)
    maxValueLn.set_data(time, maxValue)
    rmsLn.set_data(time, rms)
    minIndexLn.set_data(time[minIndex], minValue)
    maxIndexLn.set_data(time[maxIndex], maxValue)

    fft = np.abs(1/N*np.fft.fft(adc))**2
    fftAxe.set_ylim(0, np.max(fft)+0.05)
    fftAxe.set_xlim(0, fs/2)
    fftLn.set_data((fs/N)*fs*time, fft)
    return adcLn, fftLn, minValueLn, maxValueLn, rmsLn, minIndexLn, maxIndexLn


# # seleccionar si usar la biblioteca pyserial o leer desde un archivo log.bin
if(STREAM_FILE[1] == "serial"):
    streamFile = serial.Serial(
        port=STREAM_FILE[0], baudrate=230400, timeout=None)
else:
    streamFile = open(STREAM_FILE[0], "rb", 0)

ani = FuncAnimation(fig, update, 1000, init_func=None,
                    blit=False, interval=25, repeat=True)
plt.draw()
plt.show()
streamFile.close()
